﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AlignUItoGameobject : MonoBehaviour
{


    public Transform hotspotParent;
    private float hotspotoffset = 0f;
    public Transform [] HotspotTargets;
    public GameObject[] Hotspots;
    

    // Use this for initialization
    void Start()
    {


        
    }

    private void updateHotspot(Transform HotspotTransform, GameObject HotspotObject)
    {
        Vector3 hotspotTargetWorldPos = new Vector3(HotspotTransform.position.x, HotspotTransform.position.y, HotspotTransform.position.z);
        Vector3 HotspotPos = Camera.main.WorldToScreenPoint(hotspotTargetWorldPos);
        HotspotObject.transform.position = new Vector3(HotspotPos.x, HotspotPos.y, HotspotPos.z);
    }


    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < Hotspots.Length; i++)
        {
            updateHotspot(HotspotTargets[i], Hotspots[i]);
        }
        /*

        //Louvre
        if (Louvre != null)
        {
            Vector3 LouvreworldPos = new Vector3(Louvre.position.x, Louvre.position.y, Louvre.position.z);
            Vector3 LouvrePos = Camera.main.WorldToScreenPoint(LouvreworldPos);
            LouvreHotspot.transform.position = new Vector3(LouvrePos.x, LouvrePos.y, LouvrePos.z);
            float tilttolouvredistance = Vector3.Distance(TiltRodHotspot.transform.position, LouvreHotspot.transform.position);
            if (tilttolouvredistance < 150f)
            {
                LouvreHotspot.transform.Translate(-30f, 0f, 0f);
            }
        }
        else
        {
            LouvreHotspot.gameObject.SetActive(false);
        }
        if (TiltRod != null)
        {
            Vector3 TiltRodworldPos = new Vector3(TiltRod.position.x, TiltRod.position.y, TiltRod.position.z);
            Vector3 TiltRodPos = Camera.main.WorldToScreenPoint(TiltRodworldPos);
            TiltRodHotspot.transform.position = new Vector3(TiltRodPos.x, TiltRodPos.y, TiltRodPos.z);
        }
        else
        {
            TiltRodHotspot.gameObject.SetActive(false);
        }
        if (Divider != null)
        {
            Vector3 DividerworldPos = new Vector3(Divider.position.x, Divider.position.y, Divider.position.z);
            Vector3 DividerPos = Camera.main.WorldToScreenPoint(DividerworldPos);
            DividerHotspot.transform.position = new Vector3(DividerPos.x, DividerPos.y, DividerPos.z);
            DividerHotspot.transform.Translate(-50f, 0f, 0f);
        }
        else
        {
            DividerHotspot.gameObject.SetActive(false);
        }
        if (Magnets != null)
        {
            Vector3 MagnetsworldPos = new Vector3(Magnets.position.x, Magnets.position.y, Magnets.position.z);
            Vector3 MagnetsPos = Camera.main.WorldToScreenPoint(MagnetsworldPos);
            MagnetsHotspot.transform.position = new Vector3(MagnetsPos.x, MagnetsPos.y, MagnetsPos.z);
        }
        else
        {
            MagnetsHotspot.gameObject.SetActive(false);
        }
        if (Hinge != null)
        {
            Vector3 HingeworldPos = new Vector3(Hinge.position.x, Hinge.position.y, Hinge.position.z);
            Vector3 HingePos = Camera.main.WorldToScreenPoint(HingeworldPos);
            HingeHotspot.transform.position = new Vector3(HingePos.x, HingePos.y, HingePos.z);
            float hingetotiltdistance = Vector3.Distance(TiltRodHotspot.transform.position, HingeHotspot.transform.position);
            if (hingetotiltdistance < 150f)
            {
                HingeHotspot.transform.Translate(0f, 50f, 0f);
            }
        }
        else
        {
            HingeHotspot.gameObject.SetActive(false);
        }
        */
    }
}
