﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;

public class DragMouseOrbit : MonoBehaviour
{
    public EventSystem eventsystem;
    public Canvas mainCanvas;
    public Transform target;
    public Camera zoomCamera;
    public Transform pivot;
    public float distance = 2.0f;
    public float zAmount;
    public float xSpeed = 20.0f;
    public float ySpeed = 20.0f;
    public float zoomSpeed = 1.0f;      // Speed of the camera going back and forth
    public float yMinLimit = -90f;
    public float yMaxLimit = 90f;
    public float zMaxLimit = 6f;
    public float distanceMin = 10f;
    public float distanceMax = 10f;
    public float smoothTime = 0.3f;
    float rotationYAxis = 0.0f;
    float rotationXAxis = 0.0f;
    float velocityX = 0.0f;
    float velocityY = 0.0f;
    private float oldDist;
    private float dist;
    private float newDist;
    private bool trackCamera = false;
    private bool clickingOnUI = false;
    private float newZoomAmount = 20;
    public bool sliderSelected = false;

    private Vector3 mouseOrigin;    // Position of cursor when mouse dragging starts
    private bool isZooming;     // Is the camera zooming?

    // Use this for initialization
    void Start()
    {

        newZoomAmount = zoomCamera.fieldOfView;
        Vector3 angles = transform.eulerAngles;
        rotationYAxis = angles.y;
        rotationXAxis = angles.x;
        // Make the rigid body not change rotation
        if (GetComponent<Rigidbody>())
        {
            GetComponent<Rigidbody>().freezeRotation = true;
        }
    }

    public void uiSelected()
    {
        sliderSelected = true;
    }

    public void uiUnSelected()
    {
        sliderSelected = false;
    }

    public void sliderUpdateZoom(Slider Slider)
    {

        newZoomAmount = Slider.value;

    }

    void LateUpdate()
    {
        float newZoom = Mathf.SmoothDamp(zoomCamera.fieldOfView, newZoomAmount, ref zoomSpeed, smoothTime);
        zoomCamera.fieldOfView = newZoom;

        if (EventSystem.current.currentSelectedGameObject == null)
        {
            //not toch UI
            sliderSelected = false;
        }
        else
        {
            sliderSelected = true;
        }




        //Commented out zoom mouse button code
        /*
        // Get the middle mouse button
        if (Input.GetMouseButtonDown(2))
        {
            trackCamera = true;
            oldDist = Vector3.Distance(pivot.position, zoomCamera.position);
            // Get mouse origin
            mouseOrigin = Input.mousePosition;
            isZooming = true;
        }
        if (trackCamera)
        {
            dist = Vector3.Distance(pivot.position, zoomCamera.position);
            newDist = dist - oldDist;
        }

        oldDist = newDist;
        

        if (!Input.GetMouseButton(2)) isZooming = false;

        // Move the camera linearly along Z axis
        if (isZooming && !clickingOnUI)
        {


            if (newDist < dist)
            {
                if (dist >= zMaxLimit)
                {
// Do nothing
                }
                else 
                {
                    Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - mouseOrigin);
                    Vector3 move = pos.y * zoomSpeed * transform.forward;
                    zoomCamera.Translate(move, Space.World);
                }
            
            }

            else
            {
                Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - mouseOrigin);
                Vector3 move = pos.y * zoomSpeed * transform.forward;
                zoomCamera.Translate(move, Space.World);
            }
    }
     */


        if (!sliderSelected)
        {

            if (Input.GetMouseButton(0))
            {
                // Check if the mouse was clicked over a UI element
                if (EventSystem.current.IsPointerOverGameObject())
                {
                    clickingOnUI = true;
                }
                else
                {
                   // clickingOnUI = false;
                    velocityX -= xSpeed * Input.GetAxis("Mouse X") * distance * 0.02f;
                    velocityY += ySpeed * Input.GetAxis("Mouse Y") * 0.02f;
                }
            }

          //  if (clickingOnUI == false)
            
                rotationYAxis -= velocityX;
                rotationXAxis += velocityY;
               // rotationXAxis = ClampAngle(rotationXAxis, yMinLimit, yMaxLimit);
                Quaternion fromRotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, 0);
                Quaternion toRotation = Quaternion.Euler(rotationXAxis, rotationYAxis, 0);
                Quaternion rotation = toRotation;
            /*
                //Commented out Distance calc code
                distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel") * 5, distanceMin, distanceMax);
                RaycastHit hit;
                if (Physics.Linecast(target.position, transform.position, out hit))
                {
                    distance -= hit.distance;
                }
                Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
                Vector3 position = rotation * negDistance + target.position;
                */
                transform.rotation = rotation;
                //transform.position = position;
                velocityX = Mathf.Lerp(velocityX, 0, Time.deltaTime * smoothTime);
                velocityY = Mathf.Lerp(velocityY, 0, Time.deltaTime * smoothTime);
            

            }
    }

    /*
    public static float ClampAngle(float angle, float min, float max)
    {
        
        if (angle < -360F)
            angle += 360F;
        if (angle > 360F)
            angle -= 360F;
        return Mathf.Clamp(angle, min, max);
        
    }
    */
}