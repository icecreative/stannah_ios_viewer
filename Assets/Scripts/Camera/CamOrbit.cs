﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;

public class CamOrbit : MonoBehaviour {





    // variables for camera pan

    public float speedPan;



    // variables for camera zoom in and out

    public float perspectiveZoomSpeed;

    public float orthoZoomSpeed;

    public Camera mainCamera;



    //variables for camera orbit

    public Vector3 FirstPoint;

    public Vector3 SecondPoint;

    public float xAngle; //angle for axes x for rotation

    public float yAngle;

    public float xAngleTemp; //temp variable for angle

    public float yAngleTemp;

    private bool clickingOnUI = false;
    private float newZoomAmount = 20;
    public bool sliderSelected = false;
    public float zoomSpeed = 1.0f;
    public float smoothTime = 0.3f;

    // Use this for initialization

    void Start()

    {



    }
    public void uiSelected()
    {
        sliderSelected = true;
    }

    public void uiUnSelected()
    {
        sliderSelected = false;
    }

    public void sliderUpdateZoom(Slider Slider)
    {

        newZoomAmount = Slider.value;

    }


    // Update is called once per frame

    void Update()

    {
        float newZoom = Mathf.SmoothDamp(mainCamera.fieldOfView, newZoomAmount, ref zoomSpeed, smoothTime);
        mainCamera.fieldOfView = newZoom;

        if (EventSystem.current.currentSelectedGameObject == null)
        {
            //not toch UI
            sliderSelected = false;
        }
        else
        {
            sliderSelected = true;
        }



        if (!sliderSelected)
        {
            // This part is for camera pan only & for 2 fingers stationary gesture

            if (Input.touchCount > 0 && Input.GetTouch(1).phase == TouchPhase.Moved)

            {



                Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;

                transform.Translate(-touchDeltaPosition.x * speedPan, -touchDeltaPosition.y * speedPan, 0);

            }





            //this part is for zoom in and out 

            if (Input.touchCount == 2)

            {

                Touch touchZero = Input.GetTouch(0);

                Touch touchOne = Input.GetTouch(1);



                Vector2 touchZeroPreviousPosition = touchZero.position - touchZero.deltaPosition;

                Vector2 touchOnePreviousPosition = touchOne.position - touchOne.deltaPosition;



                float prevTouchDeltaMag = (touchZeroPreviousPosition - touchOnePreviousPosition).magnitude;

                float TouchDeltaMag = (touchZero.position - touchOne.position).magnitude;



                float deltaMagDiff = prevTouchDeltaMag - TouchDeltaMag;





                if (mainCamera.orthographic)

                {

                    mainCamera.orthographicSize += deltaMagDiff * orthoZoomSpeed;

                    mainCamera.orthographicSize = Mathf.Max(mainCamera.orthographicSize, .1f);

                }
                else

                {

                    mainCamera.fieldOfView += deltaMagDiff * perspectiveZoomSpeed;

                    mainCamera.fieldOfView = Mathf.Clamp(mainCamera.fieldOfView, .1f, 179.9f);

                }



            }
        }


    }

}